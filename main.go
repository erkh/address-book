package main

import (
	"bufio"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

const file string = "./addressbook.db"

const create string = `
	CREATE TABLE IF NOT EXISTS address_book (
		id INTEGER NOT NULL PRIMARY KEY,
		name TEXT,
		address TEXT)
`

const insert string = `INSERT INTO address_book (name) VALUES (?)`

type PersonType struct {
	id      int
	Name    string
	Address *string
}

type AddressBookType struct {
	People []PersonType
}

func (addressBook *AddressBookType) addPerson(newPerson PersonType) {
	addressBook.People = append(addressBook.People, newPerson)
}

var AddressBook AddressBookType

func main() {
	fmt.Printf("Welcome to the best Address book in the world!\n\n")

	db, err := sql.Open("sqlite3", file)
	checkErr(err)
	stmt, err := db.Prepare(create)
	checkErr(err)
	stmt.Exec()

	for {
		fmt.Printf("**********************\n")
		fmt.Printf("Select a command:\n\n")
		fmt.Printf("1. Add a person\n")
		fmt.Printf("2. Show people\n")
		fmt.Printf("3. Save a person\n")
		fmt.Printf("4. Show list of ppl\n")
		fmt.Printf("5. Find a person\n")
		fmt.Printf("6. Delete a person\n")
		fmt.Printf("7. Exit\n\n")

		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		input := scanner.Text()

		switch input {
		case "1":
			addPerson()
		case "2":
			showAddressBoook()
		case "3":
			savePerson()
		case "4":
			showList()
		case "5":
			findPerson(getPersonName())
		case "6":
			deletePerson(getPersonName())
		case "7":
			return
		default:
			fmt.Println("Error: input should be 1-7")
		}
	}
}

func addPerson() {
	fmt.Printf("Enter a name:\n\n")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()

	person := PersonType{
		Name: input,
	}

	AddressBook.addPerson(person)
}

func showAddressBoook() {
	b, err := json.Marshal(AddressBook)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b) + "\n")
}

func savePerson() {
	db, err := sql.Open("sqlite3", file)
	checkErr(err)
	stmt, err := db.Prepare(insert)
	checkErr(err)

	stmt.Exec(AddressBook.People[len(AddressBook.People)-1].Name)
	fmt.Printf("Added '%s' to database \n\n", AddressBook.People[len(AddressBook.People)-1].Name)
}

func showList() {
	db, err := sql.Open("sqlite3", file)
	checkErr(err)

	rows, err := db.Query("SELECT * FROM address_book")
	checkErr(err)

	people := make([]PersonType, 0)

	for rows.Next() {
		ourPerson := PersonType{}
		err = rows.Scan(&ourPerson.id, &ourPerson.Name, &ourPerson.Address)
		checkErr(err)

		people = append(people, ourPerson)
	}

	ppl, err := json.Marshal(people)
	checkErr(err)

	fmt.Println(string(ppl) + "\n")

	db.Close()
}

func getPersonName() string {
	fmt.Printf("Who are you looking for:\n\n")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	input := scanner.Text()

	return input
}

func findPerson(searchString string) {
	db, err := sql.Open("sqlite3", file)
	checkErr(err)

	rows, err := db.Query("SELECT * FROM address_book WHERE name like '%" + searchString + "%'")
	checkErr(err)

	people := make([]PersonType, 0)

	for rows.Next() {
		ourPerson := PersonType{}
		err = rows.Scan(&ourPerson.id, &ourPerson.Name, &ourPerson.Address)
		checkErr(err)

		people = append(people, ourPerson)
	}

	err = rows.Err()
	checkErr(err)

	ppl, err := json.Marshal(people)
	checkErr(err)

	fmt.Printf("Found following ppl: %v \n\n", string(ppl))
}

func deletePerson(personName string) {
	db, err := sql.Open("sqlite3", file)
	checkErr(err)

	stmt, err := db.Prepare("DELETE FROM address_book where name = ?")
	checkErr(err)

	res, err := stmt.Exec(personName)
	checkErr(err)

	affected, err := res.RowsAffected()
	checkErr(err)

	fmt.Printf("Deleted %v row, %v was deleted! \n\n", affected, personName)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
